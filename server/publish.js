Meteor.publish("robots", function () {
	return Robots.find({}, {sort: {order: 1}});
});

Meteor.publish("grid", function () {
	return Grid.find();
});

Meteor.publish("scents", function () {
	return Scents.find();
});