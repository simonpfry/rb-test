// Robot logic.

function robot (startX, startY, startDirection, instructions) {
	this.x = startX;
	this.y = startY;
	this.direction = startDirection;
	this.instructions = instructions;
	this.isLost = isRobotLost(this);

	this.initialState = {
		x: startX,
		y: startY,
		direction: startDirection,
		instructions: instructions
	}
};

createNewRobotFromStrings = function (startingConditionsString, instructionsString) {
	var parsedInitialConditions = parseStartingConditionsString(startingConditionsString);
	var newRobot = new robot(
		parsedInitialConditions.startX,
		parsedInitialConditions.startY,
		parsedInitialConditions.startDirection,
		instructionsString
	);

	return newRobot;
};

function parseStartingConditionsString (inputString) {
	var inputStringValues = inputString.split(" ");
	var startX = parseInt(inputStringValues[0], 10);
	var startY = parseInt(inputStringValues[1], 10);
	var startDirection = stringToDirection(inputStringValues[2]);

	return {
		startX: startX,
		startY: startY,
		startDirection: startDirection
	};
};

processRobot = function (robot) {
	var instructions = robot.instructions;
	if (!robot.isLost) {
		while (instructions.length > 0) {
			// Load in next instruction, remove it from queue.
			var nextInstruction = instructions.charAt(0);
			instructions = instructions.substring(1);

			// Execute instruction. More instructions can be added to the switch when required.
			switch (nextInstruction) {
				case "F":
					moveRobotForwards(robot);
					break;
				case "L":
					turnRobotLeft(robot);
					break;
				case "R":
					turnRobotRight(robot);
					break;
				default:
					Meteor.Error(500, "Internal server error", "Failure to select instruction. Bad instruction: " + nextInstruction);
			}

			// Check if robot is lost, and quit out if so.
			if (robot.isLost) {
				break;
			}
		}
	}
};

function moveRobotForwards (robot) {
	// Check to see if there is a scent at this location, and if so do ignore forward instruction.
	if (!detectScent(robot.x, robot.y, robot.direction)) {
		var grid = Grid.findOne();
		var robotCurrentPosition = {x: robot.x, y: robot.y};
		var robotFuturePosition;

		switch (robot.direction) {
			case directions.NORTH:
				robotFuturePosition = {x: robot.x, y: robot.y + 1};
				break;
			case directions.EAST:
				robotFuturePosition = {x: robot.x + 1, y :robot.y};
				break;
			case directions.SOUTH:
				robotFuturePosition = {x: robot.x, y: robot.y - 1};
				break;
			case directions.WEST:
				robotFuturePosition = {x: robot.x - 1, y: robot.y};
				break;
			default:
				Meteor.Error(500, "Internal server error", "Failure to select direction to move. Bad direction: " + robot.direction);
		}

		// Find if moving will lose the robot. If so create scent and declare robot lost. If not update robot position.
		if (willRobotBecomeLost(robotFuturePosition)) {
			robot.isLost = true;
			createScent(robotCurrentPosition.x, robotCurrentPosition.y, robot.direction);
		} else {
			robot.x = robotFuturePosition.x;
			robot.y = robotFuturePosition.y;
		}
	}
};

function turnRobotLeft (robot) {
	robot.direction = (robot.direction - 1).mod(4);
};

function turnRobotRight (robot) {
	robot.direction = (robot.direction + 1).mod(4);
};

function isRobotLost (robot) {
	var grid = Grid.findOne();
	if (grid) {
		if (robot.x < 0 || robot.x > grid.maxX || robot.y < 0 || robot.y > grid.maxY) {
			return true;
		}

		return false;
	}

	Meteor.Error(500, "Internal server error", "Grid required but not found");
};

function willRobotBecomeLost (futurePosition) {
	var grid = Grid.findOne();
	if (grid) {
		if (futurePosition.x < 0 || futurePosition.x > grid.maxX ||
		 futurePosition.y < 0 || futurePosition.y > grid.maxY) {
			return true;
		}

		return false;
	}

	Meteor.Error(500, "Internal server error", "Grid required but not found");
};