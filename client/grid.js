// Grid logic.

function grid (maxX, maxY) {
	this.maxX = maxX;
	this.maxY = maxY;
};

parseNewGridString = function (string) {
	var stringValues = string.split(" ");
	var maxX = parseInt(stringValues[0], 10);
	var maxY = parseInt(stringValues[1], 10);

	return new grid(maxX, maxY);
};