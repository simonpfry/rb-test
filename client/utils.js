// Misc. utility functions and enums.

directions = {
	NORTH: 0,
	EAST: 1,
	SOUTH: 2,
	WEST: 3
};

// Fix for mod of negative numbers.
Number.prototype.mod = function(n) {
	return ((this % n) + n) % n;
};

stringToDirection = function (string) {
	switch (string) {
		case "N":
			return directions.NORTH;
		case "E":
			return directions.EAST;
		case "S":
			return directions.SOUTH;
		case "W":
			return directions.WEST;
		default:
			Meteor.Error(500, "Internal server error", "Failure to convert string to direction. Bad string: " + string);
	}
};

directionToString = function (direction) {
	switch (direction) {
		case directions.NORTH:
			return "N";
		case directions.EAST:
			return "E";
		case directions.SOUTH:
			return "S";
		case directions.WEST:
			return "W";
		default:
			Meteor.Error(500, "Internal server error", "Failure to convert direction to string. Bad direction: " + direction);
	}
};