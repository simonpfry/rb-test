// Javascript feeding data to templates and being called from events.

Template.gridInput.events({
	'click #submitNewGrid' : function (event, template) {
		var inputString = template.find('#inputGrid').value;
		var newGrid = parseNewGridString(inputString);
		Meteor.call('upsertGrid', newGrid);
	}
});

Template.robotInput.events({
	'click #submitNewRobot' : function (event, template) {
		var startingConditionsString = template.find('#inputRobotStartingConditions').value;
		var instructionsString = template.find('#inputRobotInstructions').value;

		var newRobot = createNewRobotFromStrings(startingConditionsString, instructionsString);

		processRobot(newRobot);

		Meteor.call('insertRobot', newRobot);
	}
});

Template.displayInputs.gridMaxX = function () {
	var grid = Grid.findOne();
	if (grid) {
		return grid.maxX;
	}
};

Template.displayInputs.gridMaxY = function () {
	var grid = Grid.findOne();
	if (grid) {
		return grid.maxY;
	}
};

Template.displayInputs.robots = function () {
	return Robots.find();
};

Template.displayInputs.scents = function () {
	return Scents.find();
};

Template.displayOutputs.robots = function () {
	return Robots.find();
};

Template.displayRobotInput.startX = function () {
	return this.initialState.x;
};

Template.displayRobotInput.startY = function () {
	return this.initialState.y;
};

Template.displayRobotInput.startDirection = function () {
	return directionToString(this.initialState.direction);
};

Template.displayRobotInput.instructions = function () {
	return this.initialState.instructions;
};

Template.displayRobotOutput.direction = function () {
	return directionToString(this.direction);
};

Template.displayRobotOutput.whetherLost = function () {
	return this.isLost ? "LOST" : "";
};

Template.displayScents.direction = function () {
	return directionToString(this.direction);
};