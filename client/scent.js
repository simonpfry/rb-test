// Scent logic.

function scent (x, y, direction) {
	this.x = x;
	this.y = y;
	this.direction = direction;
};

createScent = function (x, y, direction) {
	var newScent = new scent(x, y, direction);
	Meteor.call('insertScent', newScent);
};

detectScent = function (x, y, direction) {
	// Find from scent collection whether a scent exists at this position and direction.
	var scent = Scents.findOne({x: x, y: y, direction: direction});

	return typeof scent !== 'undefined';
};