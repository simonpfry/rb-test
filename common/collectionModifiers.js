Meteor.methods({
  upsertGrid: function (grid) {
    var oldGrid = Grid.findOne();
    if (!oldGrid) {
      // No currently stored grid, so make one.
      Grid.insert(grid);
    } else {
      // Current grid exists, update it, and remove all old robots.
      Grid.update(
        oldGrid._id,
        grid);
      Robots.remove({});
      Scents.remove({});
    }
  },

  insertRobot: function (robot) {
    Robots.insert(robot);
  },

  insertScent: function (scent) {
    Scents.insert(scent);
  }
});