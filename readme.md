# rb-test
### Solution by Simon Fry

### Set up
1. Install Meteor if not installed `$ curl https://install.meteor.com/ | sh`
2. Install Meteorite if not installed `$ npm install -g meteorite`
3. Run 'mrt' from inside the cloned directory in the first instance to download the Meteorite packages.
4. Run 'meteor' in the directory from then on to start it up.

### Notes
- Specifying a new grid will remove old robots and scents.